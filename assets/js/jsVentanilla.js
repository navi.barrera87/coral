var app = angular.module('ugenerobuzon',[]);

app.controller('ventanilla',function($scope,$location,$http){
	$scope.erraa = true;
	$scope.resp = true;
	$scope.rolList = [
		{name:'Estudiante',value:'Estudiante'},
		{name:'Académico',value:'Académico'},
		{name:'Administrativo',value:'Administrativo'}
	];
	$scope.rol = $scope.rolList[0];
	$scope.arol = $scope.rolList[0];

	$scope.sexoList = [{name:'Hombre',value:'Hombre'},{name:'Mujer',value:'Mujer'}];
	$scope.sexo = $scope.sexoList[0];
	$scope.asexo = $scope.sexoList[0];

	$scope.getCampus = function(){
		$http.get('getCampus')
		.then(function(response){
			$scope.campusList = response.data;
			$scope.campus = $scope.campusList[0];
			$scope.acampus = $scope.campusList[0];
			$scope.getDivisionByCampus(2);
		});
		
	};

	$scope.getIdCampus = function(n){
		$scope.getDivisionByCampus(n);
	};

	$scope.getDivisionByCampus = function(n){
		var data = {id_campus:$scope.campus.id_campus};
		if(n==1) data = {id_campus:$scope.acampus.id_campus};
		$http.post('getDivision',data)
		.then(
			function(response){
				if(response.statusText=='OK'){
					if(n==2||n==0){
						$scope.divisionList = response.data;
						$scope.division = $scope.divisionList[0];
					}
					if(n==2||n==1){
						$scope.adivisionList = response.data;
						$scope.adivision = $scope.adivisionList[0];
					}
				}else{
					console.log('error on division select');
				}
		});
	};

	$scope.openForm = function(str){
		$scope.situacion = str;
		$scope.showAffected = ($scope.situacion=='situacion')? false:true;
		$scope.toggle = true;
		$scope.erraa = true;
		$scope.getCampus();
	};

	$scope.cancel = function(){
		$('.venForm')[0].reset();
		$scope.toggle = false;
	};

	$scope.sendReport = function(){
		if($scope.agree){
			if($scope.descripcion==null||$scope.nombres==null||$scope.apellidos==null||$scope.telefono==null||$scope.correo==null||$scope.edad==null){
				$scope.erraa = false;
				$('.error').text('Favor de llenar todos los campos...');
			}else{
				var data = {
					situacion:$scope.situacion,
					descripcion:$scope.descripcion,
					reporta: {
						nombres:$scope.nombres,
						apellidos:$scope.apellidos,
						correo:$scope.correo,
						telefono:$scope.telefono,
						rol:$scope.rol.value,
						edad:$scope.edad,
						sexo:$scope.sexo.value,
						id_campus:$scope.campus.id_campus,
						id_division:$scope.division.id_division
					}
				};
				if($scope.situacion=='situacion'&& !$scope.afectado){
					var afectado = {
						nombres:$scope.anombres,
						apellidos:$scope.aapellidos,
						correo:$scope.acorreo,
						telefono:$scope.atelefono,
						rol:$scope.arol.value,
						edad:$scope.aedad,
						sexo:$scope.asexo.value,
						id_campus:$scope.acampus.id_campus,
						id_division:$scope.adivision.id_division
					};
					data.afectado = afectado;
				}
				$http.post('sendReport',data)
				.then(
					function(response){
						$scope.resp = false;
						if(response.statusText=='OK'){
							$scope.alClass = "alert-success";
							$scope.title = "Exito!";
							$scope.message = "Se ha enviado el reporte de manera exitosa!";
							$('.venForm')[0].reset();
						}else{
							console.log('error on submitting form');
							$scope.alClass = "alert-danger";
							$scope.title = "Error!";
							$scope.message = "No se pudo enviar el reporte, favor de intentar más tarde...";
						}
				});
			}
		}else{ 
			$('.error').text('Debe aceptar los términos y condiciones...');
			$scope.erraa = false;
		}
	};
});