var app = angular.module('ugenerobuzon',[]);

app.config(function($locationProvider){
	$locationProvider.hashPrefix('');
});

app.controller('login',function($scope,$http,$location) {
	
	$scope.erraa = true;
	$scope.resp = true;
	$scope.errMsg = 'Favor de llenar todos los campos..';

	$scope.verificarUsuario = function(){
		if($scope.user!=null&& $scope.pass!=null){
			var data = {
				user: $scope.user,
				pass: $scope.pass
			};
			$http.post('administrador/verifyAdmin',data)
			.then(function(response){
				if(response.data.rol=="administrador"){
					window.location.href= 'administrador/inicio';
				}else if(response.data.indexOf('incor')>-1){
					$scope.errMsg = response.data;
					$scope.erraa = false;	
				}
			});
		}else{
			$scope.erraa = false;
		}
	};
});