var app = angular.module('ugenerobuzon',[]);

app.factory('locationService',function($http) {
	return {
		getCampus: function(){
			return $http.get('getCampus');
		},
		getDivision: function(id){
			var data = {id_campus:id};
			return $http.post('getDivision',data);
		}
	}
});

app.controller('proyectos',function($scope,$location,$http,locationService){
	$scope.erraa = true;
	$scope.resp = true;
	$scope.rolList = [
		{name:'Estudiante',value:'Estudiante'},
		{name:'Académico',value:'Académico'},
		{name:'Administrativo',value:'Administrativo'}
	];
	$scope.rol = $scope.rolList[0];

	locationService.getCampus()
	.then(function(response){
		$scope.campusList = response.data;
		$scope.campus = $scope.campusList[0];
		$scope.getIdCampus($scope.campus.id_campus);
	});

	$scope.getIdCampus = function(){
		locationService.getDivision($scope.campus.id_campus).
		then(function(response){
			$scope.divisionList = response.data;
			$scope.division = $scope.divisionList[0];
		});	
	};

	$scope.cancel = function(){
		$scope.erraa = true;
		$('.proyForm')[0].reset();

	};

	$scope.requestProject = function(){
		if($scope.agree){
			if($scope.nombres==null||$scope.apellidos==null||$scope.telefono==null||$scope.correo==null||$scope.titulo==null||$scope.objetivo==null||$scope.poblacion==null||$scope.justificacion==null||$scope.fecha==null||$scope.lugar==null||$scope.materiales==null||$scope.economicas==null||$scope.humanas==null){
				$scope.erraa = false;
				$('.error').text('Favor de llenar todos los campos...');
			}else{
				$scope.erraa = true;
				var date = new Date();
				var data = {
					reporta: {
						nombres:$scope.nombres,
						apellidos:$scope.apellidos,
						correo:$scope.correo,
						telefono:$scope.telefono,
						rol:$scope.rol.value,
						edad:0,
						sexo:0,
						id_campus:$scope.campus.id_campus,
						id_division:$scope.division.id_division
					},
					titulo: $scope.titulo,
					objetivo: $scope.objetivo,
					descripcion: $scope.descripcion,
					poblacion: $scope.poblacion,
					justificacion: $scope.justificacion,
					f_ejecucion: $scope.fecha,
					lugar: $scope.lugar,
					materiales: $scope.materiales,
					economicas: $scope.economicas,
					humanas: $scope.humanas,
					f_solicitada: date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
				};
				console.log(data);
				$http.post('sendProject',data)
				.then(
					function(response){
						$scope.resp = false;
						if(response.statusText=='OK'){
							$scope.alClass = "alert-success";
							$scope.title = "Exito!";
							$scope.message = "Se ha enviado la solicitud de manera exitosa!";
							$('.proyForm')[0].reset();
						}else{
							console.log('error on submitting form');
							$scope.alClass = "alert-danger";
							$scope.title = "Error!";
							$scope.message = "No se pudo enviar la solicitud, favor de intentar más tarde...";
						}
				});
			}
		}else{
			$scope.erraa = false;
			$('.error').text('Debe aceptar los términos y condiciones...');
		}
	};

});