var app = angular.module('coral',[]);

app.factory('reportsService',function($http){
	return {
		getReports: function(id,options){
			return $http.post('reportes/'+((id!=undefined)?id:''),options);
		},
		createReport: function(data){
			var fd = new FormData();
			for(var key in data)
				fd.append(key,data[key]);
			return $http.post('reportes/create',fd,{
				transformRequest: angular.identity,
				headers: {'Content-Type':undefined}
			});
		},
		updateReport: function(data){
			var fd = new FormData();
			for(var key in data)
				fd.append(key,data[key]);
			return $http.post('reportes/update',fd,{
				transformRequest: angular.identity,
				headers: {'Content-Type':undefined}
			});
		}
	}
});

app.factory('businessService',function($http){
	return {
		getBusiness: function(id,options){
			return $http.post('negocios/'+((id!=undefined)?id:''),options);
		}
	}
});

app.factory('helpersService',function($http){
	return {
		getClosures: function(id,options){
			return $http.post('cierres/'+((id!=undefined)?id:''),options);
		},
		getIssues: function(id,options){
			return $http.post('incidencias/'+((id!=undefined)?id:''),options);
		},
		getAttached: function(id,options){
			return $http.post('anexos/'+((id!=undefined)?id:''),options);
		},
		getInvestigated: function(id,options){
			return $http.post('investigaciones/'+((id!=undefined)?id:''),options);
		}
	}
});

app.directive('fileModel',['$parse', function($parse){
	return {
		restrict: 'A',
		link: function(scope,element, attrs){
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change',function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	}
}]);

/*app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){

	console.log(baseURL);
	$routeProvider
	.when('administrador/inicio',{
		templateURL: 'administrador/inicio',
		controller: 'InicioController'
	});
}]);

app.controller('MainCtrl',function($route, $routeParams, $location, $rootScope){
	this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;
});*/