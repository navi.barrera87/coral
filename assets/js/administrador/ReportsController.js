app.controller('ReportsController',function($scope, $http, reportsService, businessService, helpersService){
	console.log('ReportsController');

	$scope.userType = usrType;
	$scope.userId = usrId;

	var options = undefined;
	if($scope.userType=='gerente'){
		$scope.options = {
			'where' : [{field:'usuarios_id',value:$scope.userId}]
		};
	}

	$scope.monedas = [
		{name:'MXN',code:'MXN'},
		{name:'USD',code:'USD'}
	];
	$scope.xrevisions = [
		{name:'adicional',code:'adicional'},
		{name:'descontar',code:'descontar'},
		{name:'depositado',code:'depositado'}
	];
	$scope.color = {
		pendiente: '#333',
		investigacion: '#c9302c',
		revision: '#f77525',
		cerrado: '#a0a0a0'
	};
	$scope.negocios = {};
	$scope.issues = {};
	$scope.toggleDisplay = false;
	$scope.toggleResponsable = false;
	$scope.report = {};
	$scope.reportSelected = {};

	$scope.number = 1;
	$scope.getNumber = function(num) {
	    return new Array(num);   
	}

	helpersService.getClosures()
	.then(
		function (response){
			$scope.closures = response.data;
			$scope.closures.shift();
			$scope.reportSelected.cierres_id = $scope.closures[0];
		},
		function (error){
			console.log(error);
		}
	);

	helpersService.getIssues()
	.then(
		function (response){
			$scope.issues = response.data;
			$scope.issues.shift();
			$scope.reportSelected.incidencias_id = $scope.issues[0];
		},
		function (error){
			console.log(error);
		}
	);

	$scope.showReports = function(){
		reportsService.getReports()
		.then(
			function (response){
				$scope.reports = response.data;
				$scope.report.moneda = $scope.monedas[0].name;
				$scope.report.cierres_id = 1;
				$scope.report.incidencias_id = 1;

				businessService.getBusiness(undefined,$scope.options)
				.then(
					function (response){
						$scope.negocios = response.data;
						if($scope.userType=='gerente'){
							var arr = [];
							$scope.negocios.forEach(function(key){
								arr.push(key['negocios_id']);
							});

							$scope.reports = $scope.reports.filter(function(report){
								if(arr.indexOf(report.negocios_id)>-1)
									return report;
							});
						}
					},
					function (error){
						console.log(error);
					}
				);
			},
			function (error){
				console.log(error);
			}
		);
	};

	$scope.showReports();

	$scope.getNegocio = function(id){ 
		return $scope.negocios[$scope.negocios.findIndex(x => x.negocios_id == id)].nombre;
	}

	$scope.toggle = function(report){
		$scope.toggleDisplay = true;
		$scope.reportSelected = report;
		$scope.reportSelected =JSON.parse(angular.toJson($scope.reportSelected));
		$scope.incidencia = $scope.issues[$scope.issues.findIndex(x => x.incidencias_id == $scope.reportSelected.incidencias_id)].nombre;

		var options = {
			'where' : [{field:'reportes_id',value:$scope.reportSelected.reportes_id}]
		};
		helpersService.getAttached(undefined,options)
		.then(
			function (response){
				$scope.anexos = response.data;
			},
			function (error){
				console.log(error);
			}
		);
		helpersService.getInvestigated(undefined,options)
		.then(
			function (response){
				$scope.investigaciones = response.data;
			},
			function (error){
				console.log(error);
			}
		);
	};

	$scope.createReport = function(){
		var d = new Date();
		$scope.report.f_recepcion = new Date($scope.report.f_recepcion).toISOString().split('T')[0] +' '+d.toTimeString().split()[0];
		$scope.report.f_venta = new Date($scope.report.f_venta).toISOString().split('T')[0] +' '+d.toTimeString().split()[0];

		reportsService.createReport($scope.report)
		.then(
			function (response){
				$scope.reports.push(response.data);
				$scope.showReports();
			},
			function (error){
				if(error.data.tag!=undefined){
					alert(error.data.tag);
				}else{
					$.each( error.data, function( key, value ) {
  						$('#'+key).css('border','1px solid #c9302c');
					});
				}
			}
		);
	};

	$scope.closeReport = function(){
		var d = new Date();
		$scope.reportSelected.f_cierre = new Date($scope.reportSelected.f_cierre).toISOString().split('T')[0] +' '+d.toTimeString().split()[0];
		$scope.reportSelected.status = 'cerrado';

		reportsService.updateReport($scope.reportSelected)
		.then(
			function (response){
				$scope.reportSelected = response.data;
				$scope.cancelClosure();
				$scope.toggleDisplay = false;
				$scope.showReports();
			},
			function (error){
				alert(error.data.tag);
			}
		);
	};

	$scope.toInvestigation = function(status){
		$scope.reportSelected.status = status;
		var proceed = (status=='investigacion'||status=='adicional'||status=='depositado'||status=='descontar')?true:($scope.reportSelected.acta!=undefined && $scope.reportSelected.comprobante!=undefined && $scope.reportSelected.retiros!=undefined && $scope.reportSelected.personal!=undefined);
		if(proceed){
			reportsService.updateReport($scope.reportSelected)
			.then(
				function (response){
					$scope.reportSelected = response.data;
					$scope.reportSelected.status = 'investigacion';
					$scope.toggleDisplay = false;
					$scope.showReports();
				},
				function (error){
					if(error.data.tag!=undefined){
						alert(error.data.tag);
					}else{
						$.each( error.data, function( key, value ) {
	  						$('#'+key).css('border','1px solid #c9302c');
						});
					}
				}
			);
		}else{
			alert('falta documentacion o procesar el status del reporte');
		}
	};

	$scope.addMoreFiles = function(){
		if($scope.reportSelected.test0 != undefined){
			reportsService.updateReport($scope.reportSelected)
			.then(
				function (response){
					$scope.reportSelected = response.data;
					$scope.toggleDisplay = false;
					$scope.showReports();
				},
				function (error){
					alert(error.data.tag);
				}
			);
		}
		
	};

	$scope.clearReport = function(){
		$('.reportForm')[0].reset();
		$scope.report = [];
	};

	$scope.cancelClosure = function(){
		$('.closureForm')[0].reset();
		$scope.isClosed = false;
	};

	$scope.cancelInvestigate = function(){
		$('.investigateForm')[0].reset();
		$scope.isInvestigated = false;
	};

	$scope.createDocument = function(el){
		if($scope.responsable!=null||$scope.responsable!=undefined){
			$('#responsable').css('border','none');
			html2canvas(document.getElementById(el), {
		        onrendered: function (canvas) {
		            var data = canvas.toDataURL();
		            var docDefinition = {
		                content: [{
		                    image: data,
		                    width: 500,
		                }]
		            };
		            pdfMake.createPdf(docDefinition).download("acta_de_descuento_"+$scope.responsable+"_"+new Date()+".pdf");
		        }
		    });
		}else{
			$('#responsable').css('border','1px solid red');
		}
	};
});