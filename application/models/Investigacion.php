<?php
Class Investigacion extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'acta_desc', 'label'=>'acta_desc', 'rules'=>'required','errors'=>array('required'=>'acta de descuento requerido')),
		array('field'=>'comprobante', 'label'=>'comprobante', 'rules'=>'required','errors'=>array('required'=>'%s de deposito requerido')),
		array('field'=>'retiros', 'label'=>'retiros', 'rules'=>'required','errors'=>array('required'=>'reporte de %s del dia requerido')),
		array('field'=>'personal', 'label'=>'personal', 'rules'=>'required','errors'=>array('required'=>'checadas del %s requerido'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('investigaciones a');

	    if($id!=null)
	    	$this->db->where('investigaciones_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.investigaciones_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($investigacion){
		if($this->db->insert('investigaciones',$investigacion)){
			$investigacion['investigaciones_id'] = $this->db->insert_id();
			return $investigacion;
		}else
			return false;
		
	}

	public function store_batch($investigaciones){
		if($this->db->insert_batch('investigaciones',$investigaciones)){
			$investigacion['investigaciones_id'] = $this->db->insert_id();
			return $investigacion;
		}else
			return false;
	}

	public function edit($investigacion){
		$this->db->where('investigaciones_id',$investigacion['investigaciones_id']);
		if($this->db->update('investigaciones',$investigacion))
			return $investigacion;
		else
			return false;
	}

	public function delete($id){
		if(!$this->reservation->relation($id,'investigaciones_id')){
			$this->db->where('investigaciones_id',$id);
    		return ($this->db->delete('investigaciones')) ? array(true,''):array(false,'');
		}else 
			return array(false,', esta ligada a reportes');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('investigaciones');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($investigacion) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($investigacion);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	