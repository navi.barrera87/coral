<?php
Class Negocio extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'nombre', 'label'=>'nombre', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'gerentes_id', 'label'=>'gerentes_id', 'rules'=>'trim|required|integer','errors'=>array('required'=>'se requirere un gerente','integer'=>'tiene que ser un numero'))
	);

	public function read($id=null,$options=null){
		$this->db->select('*');
	    $this->db->from('negocios a');

	    if($id!=null)
	    	$this->db->where('negocios_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
		    	foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
		    }
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.'.$options['join']['table'].'_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($negocio){
		if($this->db->insert('negocios',$negocio)){
			$negocio['negocios_id'] = $this->db->insert_id();
			return $negocio;
		}else
			return false;
	}

	public function edit($negocio){
		$this->db->where('negocios_id',$negocio['negocios_id']);
		if($this->db->update('negocios',$negocio))
			return $negocio;
		else
			return false;
	}

	public function delete($id){

		$this->db->where('negocios_id',$id);
    	return $this->db->delete('negocios');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('negocios');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($negocio) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($negocio);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	