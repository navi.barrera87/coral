<?php
Class Reporte extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'f_recepcion', 'label'=>'f_recepcion', 'rules'=>'required','errors'=>array('required'=>'fecha de recepcion requerida')),
		array('field'=>'f_venta', 'label'=>'f_venta', 'rules'=>'required','errors'=>array('required'=>'fecha de venta requerida')),
		array('field'=>'deposito', 'label'=>'deposito', 'rules'=>'required','errors'=>array('required'=>'numero de deposito requerido')),
		array('field'=>'importe', 'label'=>'importe', 'rules'=>'required','errors'=>array('required'=>'%s de deposito requerido')),
		array('field'=>'diferencia', 'label'=>'diferencia', 'rules'=>'required','errors'=>array('required'=>'%s requerida')),
		array('field'=>'bancaria', 'label'=>'bancaria', 'rules'=>'required','errors'=>array('required'=>'cuenta bancaria requerida')),
		//array('field'=>'status', 'label'=>'status', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'moneda', 'label'=>'moneda', 'rules'=>'required','errors'=>array('required'=>'tipo de %s requerida')),
		array('field'=>'negocios_id', 'label'=>'negocios_id', 'rules'=>'required|trim|integer','errors'=>array('required'=>'negocio requerido','integer'=>'debe ser el id del negocio'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('reportes a');

	    if($id!=null)
	    	$this->db->where('reportes_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.reportes_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($reporte){
		if($this->db->insert('reportes',$reporte)){
			$reporte['reportes_id'] = $this->db->insert_id();
			return $reporte;
		}else
			return false;
		
	}

	public function edit($reporte){
		$this->db->where('reportes_id',$reporte['reportes_id']);
		if($this->db->update('reportes',$reporte))
			return $reporte;
		else
			return false;
	}

	public function delete($id){
		if(!$this->anexo->relation($id,'reportes_id')){
			$this->db->where('reportes_id',$id);
    		return ($this->db->delete('reportes')) ? array(true,''):array(false,'');
		}else 
			return array(false,', tiene archivos anexados');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('reportes');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($reporte) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($reporte);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	