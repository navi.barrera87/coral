<?php
Class Incidencia extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'archivo', 'label'=>'archivo', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'tipo', 'label'=>'tipo', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'reportes_id', 'label'=>'reportes_id', 'rules'=>'required|trim|integer','errors'=>array('required'=>'reporte requerido','integer'=>'debe ser el id del reporte'))
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('incidencias a');

	    if($id!=null)
	    	$this->db->where('incidencias_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.incidencias_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($incidencia){
		if($this->db->insert('incidencias',$incidencia)){
			$incidencia['incidencias_id'] = $this->db->insert_id();
			return $incidencia;
		}else
			return false;
		
	}

	public function store_batch($incidencias){
		if($this->db->insert_batch('incidencias',$incidencias)){
			$incidencia['incidencias_id'] = $this->db->insert_id();
			return $incidencia;
		}else
			return false;
	}

	public function edit($incidencia){
		$this->db->where('incidencias_id',$incidencia['incidencias_id']);
		if($this->db->update('incidencias',$incidencia))
			return $incidencia;
		else
			return false;
	}

	public function delete($id){
		$this->db->where('incidencias_id',$id);
    	return ($this->db->delete('incidencias')) ? array(true,''):array(false,'');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('incidencias');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($incidencia) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($incidencia);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	