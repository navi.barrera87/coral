<?php
Class Reservation extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->model('room','',true);
	}

	public $rules = array(
		array('field'=>'llegada', 'label'=>'llegada', 'rules'=>'required','errors'=>array('required'=>'fecha de %s requerida')),
		array('field'=>'partida', 'label'=>'partida', 'rules'=>'required','errors'=>array('required'=>'fecha de %s requerida')),
		array('field'=>'fecha', 'label'=>'fecha', 'rules'=>'required','errors'=>array('required'=>'%s de reservacion requerida')),
		array('field'=>'adultos', 'label'=>'adultos', 'rules'=>'trim|required|integer','errors'=>array('required'=>'se requirere minimo un adulto','integer'=>'tiene que ser un numero')),
		array('field'=>'ninios', 'label'=>'ninios', 'rules'=>'trim|integer','errors'=>array('integer'=>'tiene que ser un numero'))
	);

	public function read($id=null,$options=null){
		$this->db->select('*');
	    $this->db->from('reservaciones a');

	    if($id!=null)
	    	$this->db->where('reservaciones_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
		    	foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else if(isset($wheres['custom']))
		    			$this->db->where($wheres['argument']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
		    }
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.'.$options['join']['table'].'_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($reservation){
		if($this->db->insert('reservaciones',$reservation)){
			$reservation['reservaciones_id'] = $this->db->insert_id();
			return $reservation;
		}else
			return false;
	}

	public function edit($reservation){
		$this->db->where('reservaciones_id',$reservation['reservaciones_id']);
		if($this->db->update('reservaciones',$reservation))
			return $reservation;
		else
			return false;
	}

	/*public function delete($id){

		$this->db->where('id',$id);
    	return $this->db->delete('habitaciones');
	}*/

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('reservaciones');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($reservation) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($reservation);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}
	   	if(!$this->isRoomAvailable($reservation['habitaciones_id']))
	   		$errors['alert'] = 'habitacion no disponible por su estado actual';

	   	if($this->compareDates($reservation))
	   		$errors['alert'] = 'no se puede reservar, las fechas ya estan ocupadas o se traslapan';

	   	return (!empty($errors)) ? $errors: false;
	}

	private function isRoomAvailable($id){
		$room = $this->room->read($id);
		return ($room[0]['status'] == 'disponible');
	}

	private function compareDates($reservation){
		$options = array(
			'where' => array(
				array('field'=>'habitaciones_id','value'=>$reservation['habitaciones_id']),
				array('custom'=>true,'argument'=>'((llegada <="'.$reservation['llegada'].'" AND partida >= "'.$reservation['llegada']. '") OR (llegada <= "'
					.$reservation['partida'].'" AND partida >= "'.$reservation['partida'].'"))')
			)
		);
		// find reservations with proposal dates occupied or intersected
		$res = $this->read(null,$options);
		if($res){
			if(isset($reservation['reservaciones_id']) && count($res)==1)
				return !($reservation['reservaciones_id'] == $res[0]['reservaciones_id']);
			else
				return true;
		}else{
			echo 'here';
			$options = array(
				'where' => array(
					array('field'=>'habitaciones_id','value'=>$reservation['habitaciones_id']),
					array('custom'=>true,'argument'=>'(llegada >= "'.$reservation['llegada'].'" AND partida <= "'.$reservation['partida'].'")')
				)
			);
			// find reservations where dates are between de proposal reservation
			$res = $this->read(null,$options);
			return $res;
		}
	}
}	