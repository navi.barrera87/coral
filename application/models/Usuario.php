<?php
Class Usuario extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		array('field'=>'nombre', 'label'=>'nombre', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'tipo', 'label'=>'tipo', 'rules'=>'required','errors'=>array('required'=>'%s requerido')),
		array('field'=>'correo', 'label'=>'correo', 'rules'=>'required|valid_email','errors'=>array('required'=>'%s requerido','valid_email'=>'%s invalido o mal escrito')),
	);

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('usuarios a');

	    if($id!=null)
	    	$this->db->where('usuarios_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.usuarios_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($usuario){
		if($this->db->insert('usuarios',$usuario)){
			$usuario['usuarios_id'] = $this->db->insert_id();
			return $usuario;
		}else
			return false;
		
	}

	public function edit($usuario){
		$this->db->where('usuarios_id',$usuario['usuarios_id']);
		if($this->db->update('usuarios',$usuario))
			return $usuario;
		else
			return false;
	}

	public function delete($id){
		if(!$this->negocio->relation($id,'usuarios_id')){
			$this->db->where('usuarios_id',$id);
    		return ($this->db->delete('usuarios')) ? array(true,''):array(false,'');
		}else 
			return array(false,', esta ligado a negocios');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('usuarios');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function validate($usuario) {
	   	$this->load->library('form_validation');
	   	$this->form_validation->set_data($usuario);
	   	$this->form_validation->set_rules($this->rules);

	   	$errors = array();
	   	if ($this->form_validation->run() == FALSE){
	   		foreach ($this->rules as $r){
	   			if($this->form_validation->error($r['field'],' ',' ')!='')
	   				$errors[$r['field']] = $this->form_validation->error($r['field'],' ',' ');
	   		}
	   	}

	   	return (!empty($errors)) ? $errors: false;
	}
}	