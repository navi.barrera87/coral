<?php
Class Cierre extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function read($id,$options=null){
		$this->db->select('*');
	    $this->db->from('cierres a');

	    if($id!=null)
	    	$this->db->where('cierres_id',$id);

	    if(!empty($options)){
	    	if(isset($options['where'])){
	    		foreach ($options['where'] as $o => $wheres) {
		    		if(isset($wheres['like']))
		    			$this->db->like($wheres['field'],$wheres['value']);
		    		else
		    			$this->db->where($wheres['field'],$wheres['value']);
		    	}
	    	}
	    	if(isset($options['join']))
	    		$this->db->join($options['join']['table'].' z','z.cierres_id=a.'.$options['join']['table'].'_id','left');
	    	if(isset($options['sort']))
	    		$this->db->order_by($options['sort']['field'],$options['sort']['order']);
	    	if(isset($options['limit']))
	    		$this->db->limit($options['limit']['start'],$options['limit']['end']);
	    }

	    $query = $this->db->get();
	    return $query->result_array();
	}

	public function store($cierre){
		if($this->db->insert('cierres',$cierre)){
			$cierre['cierres_id'] = $this->db->insert_id();
			return $cierre;
		}else
			return false;
		
	}

	public function edit($cierre){
		$this->db->where('cierres_id',$cierre['cierres_id']);
		if($this->db->update('cierres',$cierre))
			return $cierre;
		else
			return false;
	}

	public function delete($id){
		$this->db->where('cierres_id',$id);
    	return ($this->db->delete('cierres')) ? array(true,''):array(false,'');
	}

	public function relation($id,$field){
		$this->db->where($field,$id);
		$this->db->from('cierres');
		$this->db->select('*');
		$q = $this->db->get();
		return $q->num_rows();
	}
}	