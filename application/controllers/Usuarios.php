<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('usuario','',true);
	}

	public function index($id=null){
		$options = $this->input->get();
		echo json_encode($this->usuario->read($id,$options));
	}

	public function create(){
		$validations = $this->usuario->validate($usuario);
		if(!$validations){
			$res = $this->usuario->store($usuario);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede agregar el usuario','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function update(){
		$validations = $this->usuario->validate($usuario);
		if(!$validations){
			$res = $this->usuario->edit($usuario);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede editar el usuario','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function destroy($id=null){
		$res = $this->usuario->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar el usuario'. $res[1],'tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}