<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservations extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('reservation','',true);
		$this->load->model('client','',true);
	}

	public function index($id=null){
		$options = $this->input->get();
		echo json_encode($this->reservation->read($id,$options));
	}

	public function create(){
		$validations = $this->reservation->validate($reservation);
		if(!$validations){
			$res = $this->reservation->store($reservation);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede agregar la reservacion','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function update(){
		$validations = $this->reservation->validate($reservation);
		if(!$validations){
			$res = $this->reservation->edit($reservation);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede editar la reservacion','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	/*public function destroy($id=null){
		$res = $this->client->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar el cliente'. $res[1],'tag'=>'alert'));
	}*/

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
};