<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migrations extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('migration');
		date_default_timezone_set('America/Mexico_City');
	}

	public function run(){
		$migration = $this->migration->current();
		if(!$migration)
			echo $this->migration->error_string();
		else
			echo 'Migration done' . PHP_EOL;
	}

	public function version($v){
		if(!$this->migration->version($v))
			echo $this->migration->error_string();
		else
			echo 'Migration done' . PHP_EOL;
	}
}