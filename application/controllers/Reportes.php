<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('reporte','',true);
		$this->load->model('negocio','',true);
		$this->load->model('anexo','',true);
		$this->load->model('investigacion','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->reporte->read($id,$options));
	}

	public function create(){
		$reporte = $_POST;
		if(isset($_FILES)&&!empty($_FILES)){
			$anexos = array();
			foreach ($_FILES as $file => $content) {
				$upload = $this->uploadFile($content,'anexos/');
				if($upload[0]){
					$anexos[] = array(
						'tipo' => 'auditoria',
						'archivo' => $upload[1]
					);
				}else
					return $this->getOutput(array('message'=>'error al subir archivos del reporte','tag'=>'alert'));
			}
		}
		
		$validations = $this->reporte->validate($reporte);
		if(!$validations){
			$res = $this->reporte->store($reporte);
			$data = array(
				'to' => array('navi.barrera87@gmail.com'),
				'reporte' => $reporte,
				'subject' => 'Reporte '.$res['reportes_id'].' - notificacion',
				'message' => 'Se ha levantado reporte con el folio '.$res['reportes_id'].' con estado: pendiente',
				'rol' => array('contabilidad'),
				'usr_id' => array(4) // further dev needed
			);
			if($res)
				if(isset($_FILES)&&!empty($_FILES)){
					for($i=0; $i<count($anexos);$i++)
						$anexos[$i]['reportes_id'] = $res['reportes_id'];
					if($this->anexo->store_batch($anexos)){
						$this->sendEmail($data);
						echo json_encode($res);
					}else
						return $this->getOutput(array('message'=>'error al subir archivos del reporte a BD','tag'=>'alert'));
				}else{
					$this->sendEmail($data);
					echo json_encode($res);
				}
			else
				return $this->getOutput(array('message'=>'no se puede agregar el reporte','tag'=>'alert'));
		}else{
			if(isset($_FILES)){
				$this->deleteFiles($_FILES,'anexos/');
			}
			return $this->getOutput($validations);
		}
			
	}

	public function update(){
		$reporte = $_POST;
		$folder = ($reporte['status']=='revision') ?'investigaciones/':'anexos/';

		switch($reporte['status']){
			case 'investigacion':
				$to = array('navi.barrera87@gmail.com','navi.barrera87@gmail.com');
				$rol = array('gerente','operaciones');
				$negocio = $this->negocio->read($reporte['negocios_id']);
				$usr_id = array($negocio[0]['usuarios_id'],5);
				break;
			case 'revision':
				$to = array('navi.barrera87@gmail.com');
				$rol = array('contabilidad');
				$usr_id = array(4);
				break;
			case 'cerrado':
				$to = array('navi.barrera87@gmail.com','navi.barrera87@gmail.com','navi.barrera87@gmail.com','navi.barrera87@gmail.com');
				$rol = array('gerente','operaciones','contabilidad','asistente');
				$negocio = $this->negocio->read($reporte['negocios_id']);
				$usr_id = array($negocio[0]['usuarios_id'],5,4,3);
				break;
			default:
				$to = array('navi.barrera87@gmail.com');
				$rol = array('contabilidad');
				$usr_id = array(4);
				break;
		}

		if(isset($_FILES)&&!empty($_FILES)){
			$archivos = array();
			foreach ($_FILES as $file => $content) {
				$upload = $this->uploadFile($content,$folder);
				if($upload[0]){
					$archivos[] = array(
						'tipo' => ($reporte['status']!='revision')?'adicional':'documentacion',
						'archivo' => $upload[1]
					);
				}else
					return $this->getOutput(array('message'=>'error al subir archivos del reporte','tag'=>'alert'));
			}
		}

		$validations = $this->reporte->validate($reporte);
		if(!$validations){
			$res = $this->reporte->edit($reporte);
			$data = array(
				'to' => $to,
				'reporte' => $res,
				'subject' => 'Reporte '.$res['reportes_id'].' - notificacion',
				'message' => 'El reporte con folio '.$reporte['reportes_id'].' ha pasado al estado: '.$res['status'],
				'rol' => $rol,
				'usr_id' => $usr_id // further dev needed
			);
			if($res)
				if(isset($_FILES)&&!empty($_FILES)){
					for($i=0; $i<count($archivos);$i++)
						$archivos[$i]['reportes_id'] = $res['reportes_id'];
					$add = ($res['status']!='revision')?$this->anexo->store_batch($archivos):$this->investigacion->store_batch($archivos);
					if($add){
						$this->sendEmail($data);
						echo json_encode($res);
					}else
						return $this->getOutput(array('message'=>'error al subir archivos del reporte a BD','tag'=>'alert'));
				}else{
					$this->sendEmail($data);
					echo json_encode($res);
				}
			else
				return $this->getOutput(array('message'=>'no se puede editar el reporte','tag'=>'alert'));
		}else{
			if(isset($_FILES)){
				$this->deleteFiles($_FILES,$folder);
			}
			return $this->getOutput($validations);
		}	
	}

	public function destroy($id=null){
		$res = $this->reporte->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar el reporte'. $res[1],'tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}

	private function sendEmail($data){
	    $this->load->library('email');
	    for ($i=0; $i<count($data['to']); $i++) {
		    $message = strip_tags(htmlspecialchars($data['message']));
		       
		    $email_body = 'Link/hipervinculo/liga: <a href="http://www.gotchahaven.com/coral/administrador?rol='.$data['rol'][$i].'&usr_id='.$data['usr_id'][$i].'">Ir al reporte</a>'.nl2br($message);

		    $this->email->from('contact@gotchahaven.com', 'Coral mail');
		    $this->email->to($data['to'][$i]);
		    $this->email->subject($data['subject']);
		    $this->email->message($email_body);

		    $this->email->send();
		}
	}

	function uploadFile($file,$folder){
		$targetDir = './assets/files/'.$folder;
		if (!file_exists($targetDir)){
		    mkdir($targetDir, 0777, true);
		    chmod($targetDir, 0777);
		}
	    
	    $uploadFile = '';
	    $upload_error = 'Error al cargar archivo...';

	    if (!empty($file)) {
	      	$targetDir = $targetDir . str_replace(" ","_", basename( $file["name"]));
	      	$uploadOk=true;
		    // Check if file already exists
		    if (file_exists($targetDir)) {
		        $uploadOk = false;
		        $uploadFile = 'File already exists';
		    }

	    	// Check file size
	      	if ($file["size"] > 60242880) {
	        	$uploadOk = false;
	        	$uploadFile = 'File size bigger than allowed';
	      	}

	      	// Check if $uploadOk is set to 0 by an error
	      	if (!$uploadOk) {
	        	return array($uploadOk,$uploadFile);
	      	} else {
	        	if (move_uploaded_file($file["tmp_name"], $targetDir)){
	        		$uploadFile = $file["name"];
	        		chmod($targetDir, 0777);
	          		return array($uploadOk,$uploadFile);
	        	}else return array($uploadOk,'fail');
	      	}
    	}
  	}

  	private function deleteFiles($files,$folder){
  		$targetDir = './assets/files/'.$folder;
  		foreach ($files as $file => $content) {
			if(file_exists($targetDir. str_replace(" ","_", basename( $content["name"]))))
				unlink($targetDir. str_replace(" ","_", basename( $content["name"])));
		}
  	}
}