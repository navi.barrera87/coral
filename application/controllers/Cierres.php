<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cierres extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('cierre','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->cierre->read($id,$options));
	}

	public function create(){
		$validations = $this->cierre->validate($cierre);
		if(!$validations){
			$res = $this->cierre->store($cierre);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede agregar el cierre','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function update(){
		$validations = $this->cierre->validate($cierre);
		if(!$validations){
			$res = $this->cierre->edit($cierre);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede editar el cierre','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function destroy($id=null){
		$res = $this->cierre->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar el cierre'. $res[1],'tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}