<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
		$this->load->model('usuario','',true);

		date_default_timezone_set('America/Mexico_City');

	}
	//	INICIO DE METODOS DE LOGIN
	public function index(){

		//if($this->session->userdata('logged_in')){
			$usr = $this->input->get();
			$data['rol'] = $usr['rol'];
			$data['usr_id'] = $usr['usr_id'];
			$data['scripts'] = array('administrador/ReportsController','html2canvas','pdfmake');
			$data['ngController'] = 'ReportsController';
			$this->load->view('template/vHeader',$data);
			$this->load->view('admin/vReports');
			$this->load->view('template/vFooter');
		/*}else{
			$data['scripts'] = array('jsLogin');
			$this->load->view('template/vHeader',$data);
			$this->load->view('admin/vLogin');
			$this->load->view('template/vFooter');
		}*/
	}

	/*public function verifyAdmin(){
		$obj=json_decode(file_get_contents('php://input'),true);
		if($obj['user']=='ugenerobuzonadmin'&&$obj['pass']=='ugenerobuzonadmin'){
		    $sess_array = array(
		    	'rol' => 'administrador',
		        'nombre' => 'administrador'
		    );
		    $this->session->set_userdata('logged_in', $sess_array);
		    echo json_encode($sess_array);
		    //redirect('administrador/inicio');
		}else echo 'Usuario o contraseña incorrectos...';
	}
	//	FIN DE METODOS DE LOGIN

	public function calendario(){
		//if($this->session->userdata('logged_in')){
			$data['scripts'] = array('administrador/CalendarController');
			$data['place'] = 'calendario';
			$data['ngController'] = 'CalendarController';
			$this->load->view('admin/vAdHeader',$data);
			$this->load->view('admin/vCalendar');
			$this->load->view('admin/vAdFooter');
		/*}else{
			redirect('administrador','refresh');
		}
	}*/

	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo true;
	}
}