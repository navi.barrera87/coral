<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Investigaciones extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('investigacion','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->investigacion->read($id,$options));
	}

	public function create(){
		$validations = $this->investigacion->validate($investigacion);
		if(!$validations){
			$res = $this->investigacion->store($investigacion);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede agregar la investigacion','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function update(){
		$validations = $this->usuario->validate($investigacion);
		if(!$validations){
			$res = $this->usuario->edit($investigacion);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede editar la investigacion','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function destroy($id=null){
		$res = $this->investigacion->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar la investigacion'. $res[1],'tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}