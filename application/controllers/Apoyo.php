<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apoyo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
		$this->load->model('persona','',true);
		$this->load->model('campus','',true);
		$this->load->model('ventanilla','',true);
		$this->load->model('temas','',true);
		$this->load->model('servicios','',true);
		$this->load->model('proyectos','',true);

		date_default_timezone_set('America/Mexico_City');
	}
	//		INICO DE LOS METODOS PARA VENTANILLA
	public function ventanilla(){
		$data['scripts'] = array('jsVentanilla','sweetalert2.min');
		$this->load->view('template/vHeader',$data);
		$this->load->view('publico/vVentanilla',$data);
		$this->load->view('template/vFooter',$data);
	}

	public function getCampus(){
		$res = $this->campus->getAllCampus();
		echo json_encode($res);
	}

	public function getDivision(){
		$obj=json_decode(file_get_contents('php://input'),true);
		$res = $this->campus->getDivisionByCampus($obj['id_campus']);
		echo json_encode($res);
	}

	public function sendReport(){
		$data=json_decode(file_get_contents('php://input'),true);
		$reporta = $this->persona->insertPersona($data['reporta']);
		if(isset($data['afectado']))
			$afec = $this->persona->insertPersona($data['afectado']);
		else $afec = (object) array('id_personas'=>$reporta->id_personas);
		
		if($reporta){
			$ven = array(
				'situacion'=>$data['situacion'],
				'descripcion'=>$data['descripcion'],
				'f_emitida'=>date('Y-m-d H:i:s'),
				'afectado'=>$afec->id_personas,
				'reporto'=>$reporta->id_personas,
			);
			$res = $this->ventanilla->insertVentanilla($ven);
			//cambien al metodo que funcione ya sea el primero o segundo
			$this->sendEmail('Ventanilla',$data['reporta']['correo']);
			$this->sendEmail('Ventanilla',$data['reporta']->correo);
			echo $res;
		}else echo 'ERR';
	}
	//		FIN DE LOS METODOS PARA VENTANILLA

	//		INICO DE LOS METODOS PARA SERVICIOS
	public function servicios(){
		$data['scripts'] = array('jsServicios');
		$this->load->view('template/vHeader',$data);
		$this->load->view('publico/vServicios',$data);
		$this->load->view('template/vFooter',$data);
	}

	public function getTopics(){
		$res = $this->temas->getTemasAll();
		echo json_encode($res);
	}

	public function sendRequest(){
		$data=json_decode(file_get_contents('php://input'),true);
		$reporta = $this->persona->insertPersona($data['reporta']);
		
		if($reporta){
			$ser = array(
				'poblacion'=>$data['poblacion'],
				'modalidad'=>$data['modalidad'],
				'participantes'=>$data['participantes'],
				'f_propuesta'=>$data['f_propuesta'],
				'h_propuesta'=>$data['h_propuesta'],
				'lugar'=>$data['lugar'],
				'f_solicitada'=>date('Y-m-d H:i:s'),
				'id_tema'=>$data['id_tema'],
				'id_personas'=>$reporta->id_personas,
			);
			$res = $this->servicios->insertServicio($ser);
			//cambien al metodo que funcione ya sea el primero o segundo
			$this->sendEmail('Servicios',$data['reporta']['correo']);
			$this->sendEmail('Servicios',$data['reporta']->correo);
			echo $res;
		}else echo 'ERR';
	}
	//		FIN DE LOS METODOS PARA SERVICIOS

	//		INICO DE LOS METODOS PARA PROYECTOS
	public function proyectos(){
		$data['scripts'] = array('jsProyectos');
		$this->load->view('template/vHeader',$data);
		$this->load->view('publico/vProyectos',$data);
		$this->load->view('template/vFooter',$data);
	}

	public function sendProject(){
		$data=json_decode(file_get_contents('php://input'),true);
		$reporta = $this->persona->insertPersona($data['reporta']);

		if($reporta){
			$proj = array(
				'titulo'=>$data['titulo'],
				'objetivo'=>$data['objetivo'],
				'descripcion'=>$data['descripcion'],
				'poblacion'=>$data['poblacion'],
				'justificacion'=>$data['justificacion'],
				'f_ejecucion'=>$data['f_ejecucion'],
				'lugar'=>$data['lugar'],
				'materiales'=>$data['materiales'],
				'economicas'=>$data['economicas'],
				'humanas'=>$data['humanas'],
				'f_solicitada'=>$data['f_solicitada'],
				'id_personas'=>$reporta->id_personas
			);
			$res = $this->proyectos->insertProyecto($proj);
			//cambien al metodo que funcione ya sea el primero o segundo
			$this->sendEmail('Proyecto',$data['reporta']['correo']);
			$this->sendEmail('Proyecto',$data['reporta']->correo);
			echo $res;
		}else echo 'ERR';
	}

	function sendEmail($place,$correo){
		$msg = 'En UGénero agradecemos la confianza que nos depositas al comunicarte con nosotros. En un máximo de 48 hrs. nos pondremos en contacto contigo para dar seguimiento a tu solicitud.';
		$email_address = strip_tags(htmlspecialchars($correo));
	    $message = strip_tags(htmlspecialchars($msg));
	       
	    $email_subject = 'UGénero buzón: '. $place;
	    $email_body = nl2br($message);
	    
	    $this->load->library('email'); // load email library
	    $this->email->from('ugenero@ugto.mx', 'UGénero buzón');
	    $this->email->to($email_address);
	    $this->email->subject($email_subject);
	    $this->email->message($email_body);

	    if ($this->email->send()) return true;
	    else return false;
	}
	//		INICO DE LOS METODOS PARA PROYECTOS

	/*public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo true;
	}*/
}