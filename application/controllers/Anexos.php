<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anexos extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('anexo','',true);
	}

	public function index($id=null){
		$options =json_decode(file_get_contents("php://input"),true);
		echo json_encode($this->anexo->read($id,$options));
	}

	public function create(){
		$validations = $this->anexo->validate($anexo);
		if(!$validations){
			$res = $this->anexo->store($anexo);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede agregar el anexo','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function update(){
		$validations = $this->anexo->validate($anexo);
		if(!$validations){
			$res = $this->anexo->edit($anexo);
			if($res)
				echo json_encode($res);
			else
				return $this->getOutput(array('message'=>'no se puede editar el anexo','tag'=>'alert'));
		}else
			return $this->getOutput($validations);
	}

	public function destroy($id=null){
		$res = $this->anexo->delete($id);
		if($res[0])
			echo json_encode($res[0]);
		else
			return $this->getOutput(array('message'=>'no se puede eliminar el anexo'. $res[1],'tag'=>'alert'));
	}

	public function getOutput($arr){
		return $this->output
				->set_content_type('application/json')
				->set_status_header('400')
				->set_output(json_encode($arr));
	}
}