<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_usuarios extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'usuarios_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'tipo' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'correo' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            )
        ));
        $this->dbforge->add_key('usuarios_id', TRUE);
        $this->dbforge->create_table('usuarios',TRUE);

        //Insertion values
        $usuarios = array(
            array('nombre'=>'juan','tipo'=>'gerente','correo'=>''),
            array('nombre'=>'pedro','tipo'=>'gerente','correo'=>''),
            array('nombre'=>'benito','tipo'=>'asistente','correo'=>''),
            array('nombre'=>'pablo','tipo'=>'contabilidad','correo'=>''),
            array('nombre'=>'enrique','tipo'=>'operaciones','correo'=>'')
        );
        $this->db->insert_batch('usuarios',$usuarios);
    }

    public function down(){
        $this->dbforge->drop_table('usuarios',TRUE);
    }
}