<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_reportes extends CI_Migration {

    public function up(){
        $this->load->helper('fk');
        $this->dbforge->add_field(array(
            'reportes_id' => array(
                'type' => 'INT',
                'constraint' => 6,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'f_recepcion' => array(
                'type' => 'DATETIME'
            ),
            'f_venta' => array(
                'type' => 'DATETIME'
            ),
            'f_cierre' => array(
                'type' => 'DATETIME',
                'null' => true
            ),
            'deposito' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'bancaria' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'importe' => array(
                'type' => 'FLOAT',
                'constraint' => '10,2'
            ),
            'diferencia' => array(
                'type' => 'FLOAT',
                'constraint' => '10,2'
            ),
            'moneda' => array(
                'type' => 'VARCHAR',
                'constraint' => 5
            ),
            'observaciones' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'motivo_cierre' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'pendiente'
            ),
            'negocios_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
            ),
            'cierres_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'default' => 0
            ),
            'incidencias_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('reportes_id', TRUE);
        $this->dbforge->create_table('reportes',TRUE);
        $this->db->query(add_foreign_key('reportes', 'negocios_id', 'negocios(negocios_id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key('reportes', 'cierres_id', 'cierres(cierres_id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key('reportes', 'incidencias_id', 'incidencias(incidencias_id)', 'CASCADE', 'CASCADE'));
    }

    public function down(){
        $this->load->helper('fk');
        $this->db->query(drop_foreign_key('reportes', 'negocios_id'));
        $this->db->query(drop_foreign_key('reportes', 'cierres_id'));
        $this->db->query(drop_foreign_key('reportes', 'incidencias_id'));
        $this->dbforge->drop_table('reportes',TRUE);
    }
}