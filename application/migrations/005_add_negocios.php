<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_negocios extends CI_Migration {

    public function up(){
        $this->load->helper('fk');
        $this->dbforge->add_field(array(
            'negocios_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
            'usuarios_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
            )
        ));
        $this->dbforge->add_key('negocios_id', TRUE);
        $this->dbforge->create_table('negocios',TRUE);
        $this->db->query(add_foreign_key('negocios', 'usuarios_id', 'usuarios(usuarios_id)', 'CASCADE', 'CASCADE'));

        $negocios = array(
            array('nombre'=>"porttitor eros nec",'usuarios_id'=>1),
            array('nombre'=>"semper, dui lectus",'usuarios_id'=>2),
            array('nombre'=>"Phasellus dapibus quam",'usuarios_id'=>2),
            array('nombre'=>"Aliquam adipiscing lobortis",'usuarios_id'=>1),
            array('nombre'=>"vestibulum lorem, sit",'usuarios_id'=>1),
            array('nombre'=>"vestibulum massa rutrum",'usuarios_id'=>2),
            array('nombre'=>"nec, euismod in,",'usuarios_id'=>2),
            array('nombre'=>"lorem, luctus ut,",'usuarios_id'=>1),
            array('nombre'=>"ligula elit, pretium",'usuarios_id'=>2),
            array('nombre'=>"Integer eu lacus.",'usuarios_id'=>1),
            array('nombre'=>"elit fermentum risus,",'usuarios_id'=>2),
            array('nombre'=>"Suspendisse aliquet, sem",'usuarios_id'=>2),
            array('nombre'=>"Ut sagittis lobortis",'usuarios_id'=>1),
            array('nombre'=>"nisl. Quisque fringilla",'usuarios_id'=>2),
            array('nombre'=>"sociis natoque penatibus",'usuarios_id'=>1),
            array('nombre'=>"Proin eget odio.",'usuarios_id'=>2),
            array('nombre'=>"ornare. In faucibus.",'usuarios_id'=>1),
            array('nombre'=>"tincidunt. Donec vitae",'usuarios_id'=>2),
            array('nombre'=>"ipsum. Phasellus vitae",'usuarios_id'=>2),
            array('nombre'=>"auctor ullamcorper, nisl",'usuarios_id'=>1)
        );

        $this->db->insert_batch('negocios',$negocios);
    }

    public function down(){
        $this->load->helper('fk');
        $this->db->query(drop_foreign_key('negocios', 'usuarios_id'));
        $this->dbforge->drop_table('negocios',TRUE);
    }
}