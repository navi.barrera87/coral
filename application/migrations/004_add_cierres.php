<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_cierres extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'cierres_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            )
        ));
        $this->dbforge->add_key('cierres_id', TRUE);
        $this->dbforge->create_table('cierres',TRUE);

        // Insertion values
        $cierres = array(
            array('nombre'=>'default'),
            array('nombre'=>'descontado'),
            array('nombre'=>'depositado comprobante'),
            array('nombre'=>'no se identifico responsable'),
            array('nombre'=>'sin comentarios'),
            array('nombre'=>'corto punto de venta'),
            array('nombre'=>'exedente punto de venta')
        );
        $this->db->insert_batch('cierres',$cierres);
    }

    public function down(){
        $this->dbforge->drop_table('cierres',TRUE);
    }
}