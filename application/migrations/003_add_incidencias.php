<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_incidencias extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'incidencias_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nombre' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            )
        ));
        $this->dbforge->add_key('incidencias_id', TRUE);
        $this->dbforge->create_table('incidencias',TRUE);

        // Insertion values
        $incidencias = array(
            array('nombre'=>'default'),
            array('nombre'=>'faltante'),
            array('nombre'=>'faltante no real'),
            array('nombre'=>'sobrante'),
            array('nombre'=>'sobrante no real'),
            array('nombre'=>'sin fecha'),
            array('nombre'=>'error con numero conocido'),
            array('nombre'=>'cuentas incorrectas'),
            array('nombre'=>'dolares con pesos'),
            array('nombre'=>'pesos con dolares'),
        );
        $this->db->insert_batch('incidencias',$incidencias);
    }

    public function down(){
        $this->dbforge->drop_table('incidencias',TRUE);
    }
}