<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_anexos extends CI_Migration {

    public function up(){
        $this->load->helper('fk');
        $this->dbforge->add_field(array(
            'anexos_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'archivo' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
            'tipo' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
            'reportes_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
            )
        ));
        $this->dbforge->add_key('anexos_id', TRUE);
        $this->dbforge->create_table('anexos',TRUE);
        $this->db->query(add_foreign_key('anexos', 'reportes_id', 'reportes(reportes_id)', 'CASCADE', 'CASCADE'));
    }

    public function down(){
        $this->load->helper('fk');
        $this->db->query(drop_foreign_key('anexos', 'reportes_id'));
        $this->dbforge->drop_table('anexos',TRUE);
    }
}