<div ng-controller="servicios">
	
	<div class="venTitle blueFont">
		<h3  style="text-align:center;">Bienvenid@s al catálogo de <b class="goldFont">UGÉNERO</b> </h3>
		<h4>En <b class="goldFont">UGÉNERO</b> trabajamos para incorporar la perspectiva de género en los quehaceres universitarios y 
		fomentar una cultura del respecto a los derechos humanos y la no discriminación a través de acciones de sensibilización, 
		reflexión y capacitación mediante talleres, pláticas, grupos de reflexión, conferencias y cursos que posibiliten del 
		desarrollo de habilidades, la adquisición de conocimientos y detonen actitudes de apertura, escucha y cambio. </h4>
	</div>
	<h4 class="servBlueStripe">Consulta nuestro catálogo y solicita nuestros servicios</h4>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<h4>Catálogo temático</h4>
			<ul class="list-group temasLista">
				<li class="list-group-item" ng-repeat="t in temas">
					<a ng-click="chooseTopic(t.id_tema,t.titulo)">{{t.titulo}}</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h4>Solicita nuestros servicios</h4>
			<form class="servForm" ng-submit="requestService()" novalidate>
				<section>
					<i class="fa fa-user"></i>
					<input class="user" type="text" id="nombres" ng-model="nombres" placeholder="Nombre(s)" required>
					<input class="user" type="text" id="apellidos" ng-model="apellidos" placeholder="Apellido(s)" required>
				</section>
				<section>
					<i class="fa fa-university"></i>
					<select name="campus" id="campus" ng-change="getIdCampus()" ng-model="campus" ng-options="c.nom_campus for c in campusList track by c.id_campus"></select>
					<select name="division" id="division" ng-model="division" ng-options="c.nom_division for c in divisionList track by c.id_division"></select>
				</section>
				<section class="onThird"><i class="fa fa-graduation-cap"></i><select name="rol" id="rol" ng-model="rol" ng-options="r.name for r in rolList track by r.value"></select></section>
				<section class="onThird"><i class="fa fa-phone"></i><input id="telefono" name="telefono" ng-model="telefono" type="text" placeholder="Teléfono" required></section>
				<section class="onThird"><i class="fa fa-envelope"></i><input id="correo" name="correo" ng-model="correo" type="text" placeholder="Correo electrónico" required></section>
				<section><b>Tema:</b> <input type="text" name="singleTopic" ng-model="singleTopic" placeholder="Elige tema de nuestro catálogo temático"></section>
				<section class="half"><input type="text" name="poblacion" ng-model="poblacion" placeholder="Población a quien va dirigido"></section>
				<section class="half"><input type="text" name="modalidad" ng-model="modalidad" placeholder="Modalidad de intervención que sugieres"></section>
				<section class="onThird"><i class="fa fa-users"></i><input id="participantes" name="participantes" ng-model="participantes" type="number" placeholder="# participantes" required></section>
				<section class="onThird"><i class="fa fa-calendar"></i><input id="fecha" name="fecha" ng-model="fecha" type="date" required></section>
				<section class="onThird"><i class="fa fa-map-marker"></i><input id="lugar" name="lugar" ng-model="lugar" type="text" placeholder="Lugar propuesto" required></section>
				<button class="btn btn-danger" type="button" ng-click="cancel()">Cancelar <i class="fa fa-ban"></i></button><button type="submit" class="btn btn-primary">Reportar <i class="fa fa-paper-plane"></i></button>
				<input id="agreeCheck" type="checkbox" ng-model="agree"><label for="agreeCheck"><span></span>Consiento que mis datos personales sean tratados conforme a los <a data-taget="#" ng-click="aviso=true">términos y condiciones</a> del presente aviso de privacidad.</label> 
				<br><span ng-hide="erraa" class="error">Favor de llenar todos los campos...</span><br><br>
				<div ng-hide="resp" class="alert {{alClass}} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close" ng-click="cancel()"><span aria-hidden="true">&times;</span></button>
  				<strong>{{title}}</strong> {{message}}
			</div>
			</form>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-show="aviso" style="text-align:justify;">
				<h3 style="text-align:center;">AVISO DE PRIVACIDAD</h3>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>Responsable de la protección de sus datos personales</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>La Universidad de Guanajuato a través de la Coordinación del Programa Institucional de Igualdad de Género con domicilio en Lascuráin de Retana No. 5. Zona Centro, Guanajuato, Guanajuato; así como la Unidad de Transparencia de la Universidad de Guanajuato con domicilio en Paseo Madero No. 32. Zona Centro, Guanajuato, Guanajuato.</p>
						<p>Puede contactar directamente a la Coordinación del Programa Institucional de Igualdad de Género en la dirección ya señalada, o a través del correo electrónico <b style="color:red;">ugenero@ugto.mx</b> o al teléfono 01 (473) 732 0006, ext.: 3056.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>¿Para qué fines recabamos y utilizamos sus datos personales?</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Sus datos personales serán utilizados para las siguientes finalidades:</p>
						<ol start="1">
							<li>Generar el expediente;</li>
							<li>Evaluar la calidad del servicio; </li>
							<li>Generar estadísticas; </li>
							<li>Proveer los servicios requeridos por usted, y; </li>
							<li>Realizar estudios internos sobre el ambiente institucional.</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>¿Qué datos personales obtenemos y de dónde?</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Para las finalidades señaladas en el presente aviso de privacidad, podemos recabar sus datos personales de distintas formas: cuando usted nos los proporciona directamente y cuando obtenemos información a través de otras fuentes que están permitidas por la ley.</p>
						<h5 style="text-align:center;"><b>Datos personales que recabamos de forma directa</b></h5>
						<p>Recabamos sus datos personales de forma directa cuando usted mismo nos los proporciona por diversos medios, al momento de registrarse en la página electrónica con el motivo de generar una solicitud, a través del correo electrónico o al presentarse personalmente a la Coordinación del Programa de Igualdad de Género.</p>
						<p>Los datos que obtenemos por este medio pueden ser, entre otros:</p>
						<ul>
							<li>Nombre completo</li>
							<li>Domicilio</li>
							<li>Correo electrónico</li>
							<li>Estado de salud físico o mental</li>
							<li>Origen racial o étnico</li>
							<li>Número de teléfono</li>
							<li>Número móvil</li>
							<li>Datos personales de familiares</li>
							<li>Tipo de relación con la universidad</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>Datos personales sensibles</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Le informamos que, para cumplir con las finalidades previstas en este aviso de privacidad, serán recabados y tratados datos personales sensibles, como aquellos que refieren a su estado de salud física o mental, sus ingresos económicos, así como aquellos que pongan en riesgo la intimidad de las personas.</p>
						<p>Nos comprometemos a que los mismos serán tratados bajo las más estrictas medidas de seguridad que garanticen su confidencialidad.</p>
						<p>De conformidad con lo que establece el artículo 6 fracción I de la Ley de Protección de Datos Personales para el Estado de Guanajuato, requerimos de su consentimiento para el tratamiento de sus datos personales, por lo que le solicitamos indique si acepta o no el tratamiento.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>¿Cómo corregir sus datos personales o cancelar su uso?</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Usted tiene derecho de acceder a sus datos personales que poseemos y a los detalles del tratamiento de 
						los mismos en el momento en que usted así lo considere, así como a rectificarlos en caso de ser 
						inexactos o incompletos; cancelarlos cuando considere que no se requieren para alguna de las 
						finalidades señalados en el presente aviso de privacidad, estén siendo utilizados para finalidades no 
						consentidas o haya finalizado el procedimiento del que fue sujeto(a), o bien, oponerse al tratamiento de 
						los mismos para fines específicos.</p>
						<p>Los mecanismos que se han implementado para el ejercicio de dichos derechos son a través de la presentación de la solicitud respectiva en:</p>
						<p>Ventanilla UGénero</p>
						<h5 style="text-align:center"><b>Para el Acceso de Datos Personales</b></h5>
						<p>Deberá dirigir a la Unidad de Transparencia de la Universidad de Guanajuato la solicitud correspondiente, que deberá contener:</p>
						<ol type="I">
							<li>Nombre del solicitante y domicilio para recibir notificaciones, el cual deberá ubicarse en la ciudad de Guanajuato, Guanajuato, y en caso de no señalar domicilio, las notificaciones se harán mediante un tablero que para el efecto se fije en la propia Unidad de Acceso a la Información Pública;</li>
							<li>La descripción clara y precisa de lo solicitado</li>
							<li>La modalidad en que el solicitante desee le sea entregado el informe de datos personales.</li>
						</ol>
						<h5 style="text-align:center"><b>Para la Rectificación de Datos Personales</b></h5>
						<p>Deberá dirigirse a la Unidad de Transparencia de la Universidad de Guanajuato la solicitud correspondiente, que deberá contener:</p>
						<ol type="I">
							<li>Nombre del solicitante y domicilio para recibir notificaciones, el cual deberá ubicarse en la ciudad de Guanajuato, Guanajuato, y en caso de no señalar domicilio, las notificaciones se harán mediante un tablero que para el efecto se fije en la propia Unidad de Acceso a la Información Pública;</li>
							<li>El dato o datos que se solicite corregir, mencionando la base de datos o archivo en donde consten;</li>
							<li>La acreditación de la exactitud de los datos a corregir; y</li>
							<li>En caso de que se pretenda complementar los datos personales indicar la información faltante.</li>
						</ol>
						<p>Los plazos para atender su solicitud son los siguientes:</p>
						<p>La Unidad de Transparencia de la Universidad de Guanajuato notificará al solicitante, en un plazo de 30 días hábiles contados a partir del día siguiente al de la presentación de la solicitud, las correcciones o en su caso, las razones y fundamentos por las cuales éstas no procedieron.</p>
						<h5 style="text-align:center"><b>Para la Cancelación de Datos Personales</b></h5>
						<p>Deberá presentar ante la Unidad de Transparencia de la Universidad de Guanajuato la solicitud correspondiente, quien deberá contener, al menos:</p>
						<ol type="I">
							<li>Nombre del solicitante y domicilio para recibir notificaciones, mismo que deberá estar ubicado en la ciudad de Guanajuato, Guanajuato, y en caso de no señalar domicilio, las notificaciones se harán mediante un tablero que para el efecto se fije en la propia Unidad de Transparencia; y</li>
							<li>La descripción clara y precisa de lo solicitado.</li>
						</ol>
						<p>Los plazos para atender su solicitud son los siguientes:</p>
						<p>La Unidad de Transparencia de la Universidad de Guanajuato notificará al solicitante, en un plazo de 30 días hábiles posteriores a la recepción de la solicitud.</p>
						<p>Para mayor información, favor de comunicarse a la Unidad de Transparencia al teléfono 01 (473) 732 0006, ext.: 2043</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>¿Cómo puede oponer su consentimiento para el tratamiento de sus datos?</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, a fin de que dejemos de hacer uso de los mismos. Para ello, es necesario que presente su expresión de voluntad en la Unidad de Transparencia de la Universidad de Guanajuato. Se hará mediante aviso o notificación por escrito que realizará.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>Sus datos pueden ser compartidos con otros</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Nos comprometemos a no transferir su información personal a terceros sin su consentimiento, salvo las excepciones previstas en el artículo 17 de la Ley de Protección de Datos Personales para el Estado y los Municipios de Guanajuato, así como a realizar esta transferencia en los términos que fija esa ley.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>Modificaciones al aviso de privacidad</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas, políticas internas o nuevos requerimientos para la prestación u ofrecimiento de los servicios institucionales.</p>
						<p>Estas modificaciones estarán disponibles al público a través de los siguientes medios:</p>
						<ol start="1">
							<li>En nuestra página de Internet</li>
							<li>Al último correo electrónico que nos haya proporcionado.</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<b>¿Ante quién puede presentar sus quejas y denuncias por el tratamiento indebido de sus datos personales?</b>
					</div>
					<div class="col-lg-9-col-md-9 col-sm-9 col-xs-12">
						<p>Si usted considera que su derecho de protección de datos personales ha sido lesionado por alguna conducta de algún servidor público de la Universidad de Guanajuato, presume que en el tratamiento de sus datos personales existe alguna violación a las disposiciones previstas en la Ley de Protección de Datos Personales para el Estado y los Municipios de Guanajuato, podrá interponer la queja correspondiente ante el Instituto de Acceso a la Información Pública del Estado de Guanajuato (IACIP).</p>
						<p>Para mayor información visite <a href="http://iacip-gto.org.mx/wn/">http://iacip-gto.org.mx/wn/</a></p>
					</div>
				</div>
			</div>
	</div>
</div>