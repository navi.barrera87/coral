<div ng-controller="ReportsController">
	<!--<div class="col-lg-{{(userType=='asistente'||toggleDisplay)?'6':'12'}}">-->
	<div class="col-lg-6">
		<h3>Listado de reportes <span ng-show="userType=='asistente'" class="floatRight"><button class="btn btn-primary">Reporte <i class="fa fa-plus"></i></button></span></h3>
		<div ng-repeat="r in reports" style="border-bottom:1px solid #c0c0c0;margin-bottom:4px;">
			<p>{{r.reportes_id}} /{{getNegocio(r.negocios_id)}} / {{r.f_recepcion | date : 'dd/MM/yyyy'}} / <b style="color:{{color[r.status]}};">{{r.status}}</b> <button class="btn btn-primary floatRight" ng-click="toggle(r)"><i class="fa fa-pencil"></i></button></p>
		</div>
	</div>
	<div ng-show="userType=='asistente'" class="col-lg-6">
		<form class="reportForm">
			<section>
				<b><i class="fa fa-building"></i>Negocio</b> 
				<select id="negocios_id" ng-model="report.negocios_id" ng-options="n.negocios_id as n.nombre for n in negocios">
					<option value="">Elija un negocio</option>
				</select>
			</section>
			<section><b><i class="fa fa-calendar"></i>Recepcion</b> <input type="date" name="text" id="f_recepcion" ng-model="report.f_recepcion" required></section>
			<section><b><i class="fa fa-calendar"></i>Venta</b> <input type="date" name="text" id="f_venta" ng-model="report.f_venta" required></section>
			<section><b><i class="fa fa-bookmark"></i>Deposito</b> <input type="text" id="deposito" ng-model="report.deposito" placeholder="numero de deposito"></section>
			<section><b><i class="fa fa-bank"></i>Cuenta Bancaria</b> <input type="text" id="bancaria" ng-model="report.bancaria" placeholder="cuenta bancaria"></section>
			<section><b><i class="fa fa-dollar"></i>Importe</b> <input type="number" id="importe" step="0.01" ng-model="report.importe" placeholder="0.00"></section>
			<section><b><i class="fa fa-dollar"></i>Diferencia</b> <input type="number" id="diferencia" step="0.01" ng-model="report.diferencia" placeholder="0.00"></section>
			<section>
				<b><i class="fa fa-money"></i>Moneda</b> 
				<select id="moneda" ng-model="report.moneda" ng-options="m.name as m.code for m in monedas">
				</select>
			</section>
			<section><input type="file" id="file1" name="adjunto" file-model="report.test1"></section>
			<section><input type="file" id="file2" name="adju" file-model="report.test2"></section>
			<button class="btn btn-primary" ng-click="createReport()">ENVIAR</button>
			<button class="btn btn-danger" ng-click="clearReport()">CANCELAR</button>
		</form>
	</div>
	<div ng-show="(userType!='asistente' && toggleDisplay)" class="col-lg-6">
		<h3>Reporte 
			<span class="floatRight" ng-show="(reportSelected.status!='cerrado' && userType=='contabilidad')">
				<button class="btn btn-warning" ng-click="isInvestigated = true; isClosed = false;">INVESTIGAR</button>
				<button class="btn btn-success" ng-click="isClosed = true; isInvestigated = false;">CERRAR</button>
			</span>
		</h3>
		<section><b><i class="fa fa-calendar"></i> Recepcion</b> {{reportSelected.f_recepcion | date: yyy-mm-dd}}</section>
			<section><b><i class="fa fa-calendar"></i> Venta</b> {{reportSelected.f_venta | date: yyy-mm-dd}}</section>
			<section><b><i class="fa fa-building"></i> Negocio</b> {{getNegocio(reportSelected.negocios_id)}}</section>
			<section><b><i class="fa fa-bookmark"></i> Deposito</b> {{reportSelected.deposito}}</section>
			<section><b><i class="fa fa-bank"></i> Cuenta Bancaria</b> {{reportSelected.bancaria}}</section>
			<section><b><i class="fa fa-dollar"></i> Importe</b> {{reportSelected.importe}}</section>
			<section><b><i class="fa fa-dollar"></i> Diferencia</b> {{reportSelected.diferencia}}</section>
			<section><b><i class="fa fa-money"></i> Moneda</b> {{reportSelected.moneda}}</section>
			<section ng-show="reportSelected.incidencias_id!=1"><b><i class="fa fa-exclamation-triangle"></i> Incidencia</b> {{incidencia}}</section>
			<section><b><i class="fa fa-clipboard"></i> Status</b> {{reportSelected.status}}</section>
			<section ng-show="reportSelected.status=='cerrado'"><b><i class="fa fa-calendar"></i> Cierre</b> {{reportSelected.f_cierre | date: yyy-mm-dd}}</section>
		<section ng-show="anexos.length > 0">
			<h4>Archivos anexados</h4>
			<a ng-repeat="a in anexos" target="_blank" href="./assets/files/anexos/{{a.archivo}}" style="margin-right:8px;"><i class="fa fa-file"></i> {{a.archivo}}</a>
		</section>
		<section ng-show="investigaciones.length > 0">
			<h4>Archivos de investigacion para revision</h4>
			<a ng-repeat="a in investigaciones" target="_blank" href="./assets/files/investigaciones/{{a.archivo}}" style="margin-right:8px;"><i class="fa fa-file"></i> {{a.archivo}}</a>
		</section>
		<section class="threeQuarter" ng-repeat="i in getNumber(number) track by $index" ng-show="userType!='gerente'">
			<input type="file" file-model="reportSelected.test{{$index}}">
		</section>
		<button class="btn btn-info" ng-show="reportSelected.status=='cerrado'" ng-click="addMoreFiles()">Adjuntar</button>
		<button class="btn btn-primary" ng-click="number = number+1" ng-show="userType!='gerente'"><i class="fa fa-plus"></i></button>
		<button class="btn btn-danger" ng-click="number = number-1" ng-show="userType!='gerente'"><i class="fa fa-minus"></i></button>
		<form ng-show="isClosed" class="closureForm" ng-show="userType=='contabilidad'">
			<h3>Cierre del Reporte </h3>
			<section><b>Causa del cierre</b><select ng-model="reportSelected.cierres_id" ng-options="c.cierres_id as c.nombre for c in closures"></select></section>
			<section><b><i class="fa fa-calendar"></i>Fecha de cierre</b> <input type="date" name="text" ng-model="reportSelected.f_cierre"></section>
			<section><b>Motivo del cierre (opcional)</b><textarea cols="30" rows="10" ng-model="reportSelected.motivo_cierre" placeholder="escriba el motivo"></textarea></section>
			<button class="btn btn-danger" ng-click="cancelClosure()">CANCELAR</button>
			<button class="btn btn-success" ng-click="closeReport()">CERRAR</button>
		</form>
		<form ng-show="isInvestigated" class="investigateForm" ng-show="userType=='contabilidad'">
			<h3>Investigacion del Reporte </h3>
			<section><b>Incidencia</b><select ng-model="reportSelected.incidencias_id" ng-options="i.incidencias_id as i.nombre for i in issues"></select></section>
			<button class="btn btn-danger" ng-click="cancelInvestigate()">CANCELAR</button>
			<button class="btn btn-warning" ng-click="toInvestigation('investigacion')">INVESTIGAR</button>
		</form>
		<form ng-show="(userType=='operaciones' && reportSelected.status == 'revision')">
			<h3>Revision del Reporte </h3>
			<section><b>Cambiar status a:</b><select ng-model="xrevision" ng-options="m.name as m.code for m in xrevisions"></select></section>
			<button class="btn btn-info" ng-click="toInvestigation(xrevision)">MANDAR</button>
		</form>
		<form class="investigationForm" ng-show="(userType=='gerente'&&reportSelected.status=='investigacion')">
			<h3>Reporte en investigacion</h3>
			<section>
				<b>Acta de descuento</b>
				<input type="file" file-model="reportSelected.acta" style="width:60%; display:inline-block;">
				<button class="btn btn-info" ng-click="toggleResponsable = true">ACTA</button><br><br>
				<input type="text" id="responsable" ng-model="responsable" placeholder="Nombre del responsable" ng-show="toggleResponsable" style="width:55%; display:inline-block;">
				<button class="btn btn-primary" ng-click="createDocument('template')" ng-show="toggleResponsable">GENERAR</button>
				<button class="btn btn-danger" ng-click="toggleResponsable = false" ng-show="toggleResponsable">CANCELAR</button>
				<div id="template" ng-show="toggleResponsable">
					<section><b><i class="fa fa-calendar"></i> Responsable</b> {{responsable}}</section><br>
					<section><b><i class="fa fa-calendar"></i> Recepcion</b> {{reportSelected.f_recepcion | date: yyy-mm-dd}}</section><br>
					<section><b><i class="fa fa-calendar"></i> Venta</b> {{reportSelected.f_venta | date: yyy-mm-dd}}</section><br>
					<section><b><i class="fa fa-dollar"></i> Diferencia</b> {{reportSelected.diferencia}}</section><br><br><br><br>
					<div style="display:inline-block; width:30%; text-align:center;"><span style="border-top:1px solid #333;">Gerente del Negocio</span></div>
					<div style="display:inline-block; width:30%; text-align:center;"><span style="border-top:1px solid #333;">Recursos Humanos</span></div>
					<div style="display:inline-block; width:30%; text-align:center;"><span style="border-top:1px solid #333;">Testigo</span></div>
				</div>
			</section>
			<section><b>Comprobante de deposito</b><input type="file" file-model="reportSelected.comprobante"></section>
			<section><b>Reporte de retiros al dia</b><input type="file" file-model="reportSelected.retiros"></section>
			<section><b>Checadas del personal</b><input type="file" file-model="reportSelected.personal"></section>
			<button class="btn btn-success" ng-click="toInvestigation('revision')">ENVIAR</button>
		</form>
	</div>
	
</div>