<div ng-controller="login">
	<form id="loginForm" class="loginForm col-md-4 col-md-offset-4 col-lg-offset-4" name="loginForm" ng-submit="verificarUsuario()" novalidate>
		<h3 class="blueFont">ACCESO ADMINISTRADOR</h3>
		<section>
			<b><i class="glyphicon glyphicon-user"></i></b> 
			<input type="text" name="user" id="user" ng-model="user" placeholder="Usuario" required>
		</section>
		<section>
			<b><i class="glyphicon glyphicon-lock"></i></b> 
			<input type="password" name="pass" id="pass" ng-model="pass" placeholder="Contraseña" required>
		</section>
		<span ng-hide="erraa" class="error">{{errMsg}}</span><br>
		<button type="submit" class="btn btn-primary">
			<b>Entrar</b> <i class="glyphicon glyphicon-circle-arrow-right"></i>
		</button>
	</form>
</div>