    </div>
    <footer class="container-fluid">
    	<nav class="nav col-lg-4 col-lg-offset-2 col-sm-8 col-xs-12 nav-footer">
            <ul class="nav navbar-nav col-lg-12 col-xs-12">
                <li class="col-xs-2"><a href="#" ng-click="refresh()"><i class="fa fa-refresh"></i><span class="menutitle">Actualizar</span></a></li>
                <li class="col-xs-10 search">
                	<a href="#" ng-click="search()">
                		<i class="fa fa-search"></i>
                		<span class="menutitle">Buscar</span>
                	</a>
                	<input type="text">
                </li>
            </ul>
        </nav>
    	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 logdata">
    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-7"><i class="fa fa-user"></i> <b>Usuario actual</b></div>
    		<a class="col-lg-12 col-md-12 col-sm-12 col-xs-5" href="administrador/logout">Cerrar sesión <i class="fa fa-sign-out"></i></a>
    	</div>
    </footer>
</body>
</html>